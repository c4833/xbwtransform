#!/usr/bin/env python3
import argparse

from parser_commands.bwt.BWTDecodeCommand import BWTDecodeCommand
from parser_commands.bwt.BWTEncodeCommand import BWTEncodeCommand
from parser_commands.bwt.PlotBWTRLECommand import PlotBWTRLECommand
from parser_commands.wrap_command import wrap_command


def _build_parser():
    parser = argparse.ArgumentParser(
        description='BWT coding and decoding'
    )
    subparsers = parser.add_subparsers(
        dest='subcommand', required=True
    )
    commands = (
        BWTEncodeCommand,
        BWTDecodeCommand,
        PlotBWTRLECommand
    )

    for c in commands:
        p = c.set_up_parser(
            subparsers.add_parser(
                c.COMMAND_NAME,
                description=c.DESCRIPTION
            )
        )
        p.set_defaults(func=wrap_command(c))

    return parser


def main():
    parser = _build_parser()
    args = parser.parse_args()

    return args.func(args)


if __name__ == "__main__":
    exit(main())
