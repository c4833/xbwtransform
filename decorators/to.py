def to(transformer):
    def a(f):
        def b(*args, **kwargs):
            return transformer(f(*args, **kwargs))

        return b

    return a
