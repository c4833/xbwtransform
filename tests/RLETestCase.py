from tests.BaseTestCase import BaseTestCase
from utils.rle import rle


class RLETestCase(BaseTestCase):
    def test_rle(self):
        inputs = (
            '',
            'a',
            'ab',
            'abc',
            'aaabbb',
            'aaabbbc',
            'aaabbbcd',
            'abcddd'
        )
        expected_outputs = (
            '',
            'a',
            'ab',
            'abc',
            '3a3b',
            '3a3bc',
            '3a3bcd',
            'abc3d'
        )

        for i, e in zip(inputs, expected_outputs):
            self.assertEqual(e, rle(i))
