from core.xbw.BytesPacker import BytesPacker
from tests.BaseTestCase import BaseTestCase


class ArchiveTestCase(BaseTestCase):
    def test_pack_unpack(self):
        _input = (
            (b'hello', b'how', b'is', b'it', b'?'),
            (b'test including a newline\ncharacter', b'let\'s', b'see')
        )
        a = BytesPacker()

        for i in _input:
            self.assertSequenceEqual(
                i, tuple(a.unpack(a.pack(*i)))
            )
