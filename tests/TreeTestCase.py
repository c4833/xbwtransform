import unittest
from core.tree.Tree import Tree


class TreeTestCase(unittest.TestCase):
	"""
	Test cases for the tree class.
	"""
	def setUp(self):
		self._tree = Tree(0)

		self._c1 = self._tree.append_child(Tree(1))
		self._c2 = self._tree.append_child(Tree(2))
		self._c3 = self._tree.append_child(Tree(3))

		self._c11 = self._c1.append_child(Tree(4))
		self._c12 = self._c1.append_child(Tree(5))
		self._c13 = self._c1.append_child(Tree(6))

	def test_value_getter(self):
		self.assertEqual(self._tree.value, 0)

	def test_value_setter(self):
		self._tree.value = 42
		self.assertEqual(self._tree.value, 42)

	def test_parent(self):
		self.assertEqual(self._tree.parent, None)

		for child in self._tree.children:
			self.assertEqual(child.parent, self._tree)

	def test_first_child(self):
		self.assertEqual(self._tree.first_child, self._c1)

	def test_last_child(self):
		self.assertEqual(self._tree.last_child, self._c3)

	def test_children(self):
		self.assertEqual(
			tuple(self._tree.children), (self._c1, self._c2, self._c3)
		)
		self.assertEqual(
			tuple(self._c1.children), (self._c11, self._c12, self._c13)
		)

	def test_append_child(self):
		pass

	def test_remove_child(self):
		self._tree.remove_child(self._c2)

		self.assertEqual(
			(self._c1, self._c3), tuple(self._tree.children)
		)
		self.assertIsNone(self._c2.parent)
		self.assertIsNone(self._c2.next_sibling)
		self.assertEqual(self._c1.next_sibling, self._c3)

		# Remove c1, and see what happens
		self._tree.remove_child(self._c1)

		self.assertEqual(
			(self._c3,), tuple(self._tree.children)
		)
		self.assertIsNone(self._c1.parent)
		self.assertIsNone(self._c1.next_sibling)
		self.assertEqual(self._c3.next_sibling, None)
		self.assertEqual(self._tree.first_child, self._c3)

		# Remove c3, and see what happens
		self._tree.remove_child(self._c3)

		self.assertEqual(
			tuple(), tuple(self._tree.children)
		)
		self.assertIsNone(self._c3.parent)
		self.assertIsNone(self._c3.next_sibling)
		self.assertEqual(self._c3.next_sibling, None)
		self.assertEqual(self._tree.first_child, None)

	def test_next_sibling(self):
		self.assertEqual(self._c1.next_sibling, self._c2)
		self.assertEqual(self._c2.next_sibling, self._c3)
		self.assertEqual(self._c3.next_sibling, None)

	def test_str(self):
		expected = """\
0
	1
		4
		5
		6
	2
	3\
"""
		# print(str(self._tree))
		self.assertEqual(
			str(self._tree), expected
		)


if __name__ == '__main__':
	unittest.main()
