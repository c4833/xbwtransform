from core.tree.SimpleTreeReader import SimpleTreeReader
from core.xbw.spine import spines
from core.xbw.spine.LeftSpineAutomaton import LeftSpineAutomaton
from core.xbw.spine.RightSpineAutomaton import RightSpineAutomaton
from core.xbw.PreOrderedXBWTransform import PreOrderedXBWTransform
from tests.BaseTestCase import BaseTestCase


class SpineTestCase(BaseTestCase):
	TEST_DIR = 'shared'

	@classmethod
	def _test_path(cls, *args):
		return super()._test_path(cls.TEST_DIR, *args)

	def test_left_spines(self):
		reader = SimpleTreeReader()
		input_files = (
			'tree01.stree',
			'tree02.stree',
			'tree03.stree',
			'tree04.stree',
			'tree05-left-spine.stree',
			'tree06-right-spine.stree',
			'tree07-not-a-left-spine.stree',
			'tree08.stree',
			'tree09-short-left-spine.stree',
			'tree10.stree',
			'tree11.stree',
			'tree12.stree',
			'tree13.stree',
			'tree14.stree',
			'tree15.stree',
			'tree16.stree',
		)
		expected = (
			((4, 12), ),
			((4, 12), ),
			((4, 12), (15, 27)),
			tuple(),
			((0, 8), ),
			((6, 8), ),  # A right spine, with a small left spine at the end
			tuple(),
			((4, 6), (7, 9), (10, 18)),
			((4, 6), ),
			((2, 6), (7, 13)),
			((6, 8), ),
			tuple(),
			((7, 9), ),
			((4, 6), (11, 13), ),
			((7, 9), ),
			((6, 8), )
		)

		for e, file_name in zip(expected, input_files):
			full_path = self._test_path(file_name)
			automaton = LeftSpineAutomaton(
				PreOrderedXBWTransform.from_tree(
					reader.readfile(full_path)
				)
			)

			self.assertEqual(
				e, tuple(spines(automaton)), 'Failure at %s' % file_name
			)

	def test_right_spines(self):
		reader = SimpleTreeReader()
		input_files = (
			'tree01.stree',
			'tree02.stree',
			'tree03.stree',
			'tree04.stree',
			'tree05-left-spine.stree',
			'tree06-right-spine.stree',
			'tree07-not-a-left-spine.stree',
			'tree08.stree',
			'tree09-short-left-spine.stree',
			'tree10.stree',
			'tree11.stree',
			'tree12.stree',
			'tree13.stree',
			'tree14.stree',
			'tree15.stree',
			'tree16.stree',
		)
		expected = (
			((7, 9), ),
			((7, 9), ),
			((7, 9), (20, 22)),
			tuple(),
			((3, 5), ),
			((0, 8), ),
			tuple(),
			((4, 6), (7, 9), (13, 15)),
			((4, 6), ),
			((3, 5), (9, 11)),
			((2, 8), ),
			tuple(),
			((3, 9), ),
			((4, 6), (7, 13)),
			((3, 9), ),
			((2, 8), )
		)

		for e, file_name in zip(expected, input_files):
			full_path = self._test_path(file_name)
			automaton = RightSpineAutomaton(
				PreOrderedXBWTransform.from_tree(
					reader.readfile(full_path)
				)
			)

			self.assertEqual(
				e, tuple(spines(automaton)), 'Failure at %s' % file_name
			)
