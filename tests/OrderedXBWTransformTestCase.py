import gzip
import os
import unittest
from unittest import skip

from core.tree.SimpleTreeReader import SimpleTreeReader
from core.xbw.codec.XBWTransformCodecBundle import \
    XBWTransformCodecBundle
from core.xbw.OrderedXBWTransform import OrderedXBWTransform
from core.xbw.BaseXBWEntry import BaseXBWEntry as E
from tests.BaseTestCase import BaseTestCase


class OrderedXBWTransformTestCase(BaseTestCase):
    XBW_TEST_DIR = BaseTestCase._test_path('TestXBW')

    def test_xbw_transform(self):
        expected = (
            E(0, 'R', ''),
            E(0, 'd', 'AR', 1),
            E(0, 'c', 'AR', 1),
            E(1, 'e', 'AR', 1),
            E(0, 'A', 'R'),
            E(0, 'b', 'R', 1),
            E(1, 'a', 'R', 1),
        )
        tree = SimpleTreeReader().readfile(
            self._test_path(self.SHARED_TEST_DIR, 'tree19.stree')
        )

        self.assertEqual(
            expected, tuple(OrderedXBWTransform.from_tree(tree))
        )

    def test_ferragina_tree(self):
        expected = (
            E(0, 'A', ''),
            E(0, 'B', 'A'),
            E(0, 'C', 'A'),
            E(1, 'B', 'A'),
            E(0, 'D', 'BA'),
            E(0, 'a', 'BA', 1),
            E(1, 'E', 'BA'),
            E(1, 'D', 'BA'),
            E(0, 'D', 'CA'),
            E(0, 'b', 'CA', 1),
            E(1, 'D', 'CA'),
            E(1, 'a', 'DBA', 1),
            E(1, 'b', 'DBA', 1),
            E(1, 'c', 'DCA', 1),
            E(1, 'c', 'DCA', 1),
            E(1, 'b', 'EBA', 1)
        )
        tree = SimpleTreeReader().readfile(
            self._test_path(self.SHARED_TEST_DIR, 'tree18-ferragina.stree')
        )

        self.assertEqual(tuple(OrderedXBWTransform.from_tree(tree)), expected)

    @skip
    def test_to_tree(self):
        reader = SimpleTreeReader()

        for tree_name in os.listdir(self._test_path(self.SHARED_TEST_DIR)):
            expected = reader.readfile(
                self._test_path(self.SHARED_TEST_DIR, tree_name)
            )
            transform = OrderedXBWTransform.from_tree(expected)

            self.assertEqual(
                expected, transform.to_tree(),
                'Failure at %s' % tree_name
            )

    @skip
    def test_compress_decompress(self):
        reader = SimpleTreeReader()
        codec = XBWTransformCodecBundle(
            gzip.compress, gzip.decompress,
            gzip.compress, gzip.decompress,
        )

        for tree_name in os.listdir(self._test_path(self.SHARED_TEST_DIR)):
            expected = OrderedXBWTransform.from_tree(
                reader.readfile(
                    self._test_path(self.SHARED_TEST_DIR, tree_name)
                )
            )

            self.assertEqual(
                expected.decompress(expected.compress(codec), codec),
                expected,
                'Failure at %s' % tree_name
            )


if __name__ == '__main__':
    unittest.main()
