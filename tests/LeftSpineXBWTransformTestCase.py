import gzip

from core.tree.SimpleTreeReader import SimpleTreeReader
from core.xbw.PrunedXBWTransform.LeftSpineXBWTransform import \
    LeftSpineXBWTransform
from core.xbw.codec.SpineCodecBundle import SpineCodecBundle
from tests.BaseTestCase import BaseTestCase


class LeftSpineXBWTransformTestCase(BaseTestCase):
    def test_raises_value_error(self):
        _input = (
            'tree01.stree',
            'tree02.stree',
            'tree03.stree',
            'tree04.stree'
        )
        reader = SimpleTreeReader()

        for i in _input:
            self.assertRaises(
                ValueError,
                LeftSpineXBWTransform.from_tree,
                reader.readfile(
                    self._test_path(self.SHARED_TEST_DIR, i)
                )
            )

    def test_compress_decompress(self):
        _input = (
            'tree05-left-spine.stree',
        )
        reader = SimpleTreeReader()
        codec = SpineCodecBundle(gzip.compress, gzip.decompress)

        for i in _input:
            spine = LeftSpineXBWTransform.from_tree(
                reader.readfile(
                    self._test_path(self.SHARED_TEST_DIR, i)
                )
            )

            self.assertEqual(
                spine,
                spine.decompress(spine.compress(codec), codec)
            )
