import os
import shutil
from argparse import ArgumentParser

from parser_commands.lz.LCFUtils import LCFUtils
from parser_commands.lz.WriteLCFsCommand import WriteLCFsCommand
from parser_commands.wrap_command import wrap_command
from tests.BaseTestCase import BaseTestCase


class WriteLCFsCommandTestCase(BaseTestCase):
    """
    Tests that WriteLCFsCommand effectively writes the
    Logarithmically-Compressible Files as expected
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        parser = ArgumentParser()
        subparsers = parser.add_subparsers()

        WriteLCFsCommand.set_up_parser(
            subparsers.add_parser(
                WriteLCFsCommand.COMMAND_NAME
            )
        ).set_defaults(
            func=wrap_command(WriteLCFsCommand)
        )

        self._args = parser.parse_args((
            'write_lcfs',
            '--debug',
            '-b', '1024',
            '-l', '512',
            '-c', '10',
            '-cw', '5',
            '10:21',
            self.out_dir()
        ))

    @classmethod
    def out_dir(cls):
        return cls._test_path(cls.__name__)

    def setUp(self):
        # Create the directory under which to run the WriteLCFsCommand
        os.mkdir(self.out_dir())
        # Run the WriteLCFsCommand
        self._args.func(self._args)

    def tearDown(self):
        shutil.rmtree(self.out_dir())

    def test_lcfs(self):
        for e in self._args.exponents:
            lcf_path = os.path.join(
                self.out_dir(), LCFUtils.format(e)
            )

            with open(lcf_path) as lcf_file:
                self.assertWellFormedLCF(lcf_file)

    def assertWellFormedLCF(self, lcf):
        expected_chunk_len = (
                self._args.buffer - self._args.lookahead_buffer
        ) / self._args.chunks_per_window
        length_delta = 5
        chunks = set(
            lcf.read().split(WriteLCFsCommand.SEPARATOR)
        )

        # Check that the number of distinct chunks does not exceed the
        # command-line-provided value
        self.assertLessEqual(len(chunks), self._args.chunks)

        for c in chunks:
            # Assert that the length of each individual chunk is within a given
            # margin
            self.assertLessEqual(
                abs(expected_chunk_len - len(c)), length_delta
            )
            # Assert that each chunk is composed by a single character
            self.assertEqual(len(set(c)), 1)
