import unittest

from core.compression.LZ77Codec import LZ77Codec
from tests.BaseTestCase import BaseTestCase


class LZ1TestCase(BaseTestCase):
    def test_constructor(self):
        self.assertRaises(
            ValueError, LZ77Codec, buffer=100, look_ahead=200
        )

    def test_compression_decompression(self):
        _input = (
            '',
            2000 * 'a',
            1000 * 'a' + 1000 * 'b',
            1000 * 'ab',
            1000 * 'a' + 1000 * 'b' + 1000 * 'c',
            2000 * 'hello'
        )
        codec = LZ77Codec()

        for i in _input:
            self.assertEqual(
                i, codec.decompress(''.join(codec.compress(i)))
            )


if __name__ == '__main__':
    unittest.main()
