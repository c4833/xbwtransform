from core.tree.SimpleTreeReader import SimpleTreeReader
from core.tree.SpineUtils import build_left_spine, build_right_spine
from core.xbw.PreOrderedXBWTransform import PreOrderedXBWTransform
from tests.BaseTestCase import BaseTestCase


class SpineUtilsTestCase(BaseTestCase):
    def test_build_left_spine(self):
        reader = SimpleTreeReader()
        expected = reader.readfile(
            self._test_path(self.SHARED_TEST_DIR, 'tree05-left-spine.stree')
        )
        transform = PreOrderedXBWTransform.from_tree(expected)

        self.assertEqual(
            expected, build_left_spine(transform.labels)
        )

    def test_build_left_spine_empty_argument(self):
        self.assertIsNone(build_left_spine(''))

    def test_build_left_spine_even_length(self):
        self.assertRaises(
            ValueError, build_left_spine, 'aabbcc'
        )

    def test_build_right_spine(self):
        reader = SimpleTreeReader()
        expected = reader.readfile(
            self._test_path(self.SHARED_TEST_DIR, 'tree06-right-spine.stree')
        )
        transform = PreOrderedXBWTransform.from_tree(expected)

        self.assertEqual(
            expected, build_right_spine(transform.labels)
        )

    def test_build_right_spine_empty_argument(self):
        self.assertIsNone(build_right_spine(''))

    def test_build_right_spine_even_length(self):
        self.assertRaises(
            ValueError, build_right_spine, 'aabbcc'
        )
