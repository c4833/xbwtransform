import unittest

from core.tree.SimpleTreeReader import SimpleTreeReader
from core.tree.Tree import Tree
from tests.BaseTestCase import BaseTestCase


def build_linear_tree(root, tokens):
	old_root = root

	for new_root, leaf in tokens:
		new_root, leaf = Tree(new_root), Tree(leaf)

		root.append_child(new_root)
		root.append_child(leaf)

		root = new_root

	return old_root


class SimpleTreeReaderTestCase(BaseTestCase):
	TEST_DIR = BaseTestCase._test_path('SimpleTreeReader')

	@classmethod
	def _test_path(cls, *args):
		return super()._test_path(cls.TEST_DIR, *args)

	def test_tree_2(self):
		expected = Tree(
			'R',
			Tree(
				'B',
				Tree('b')
			),
			build_linear_tree(Tree('D'), (
				('B', 'a'),
				('C', 'b'),
				('D', 'c'),
				('e', 'd'),
			)),
			Tree(
				'E',
				Tree('a'),
				Tree(
					'B',
					Tree('C', Tree('f')),
					Tree('D', Tree('e'))
				)
			),
		)
		reader = SimpleTreeReader()
		tree_2_path = self._test_path('tree02.stree')

		self.assertEqual(reader.readfile(tree_2_path), expected)


if __name__ == '__main__':
	unittest.main()
