import unittest

from core.tree.SimpleTreeReader import SimpleTreeReader
from core.xbw.PreOrderedXBWTransform import PreOrderedXBWTransform
from core.xbw.BaseXBWEntry import BaseXBWEntry as E
from tests.BaseTestCase import BaseTestCase


class PreOrderedXBWTransformTestCase(BaseTestCase):
    XBW_TEST_DIR = BaseTestCase._test_path('TestXBW')

    def test_xbw_transform(self):
        expected = (
            E(0, 'R', ''),
            E(0, 'A', 'R'),
            E(0, 'd', 'AR'),
            E(0, 'c', 'AR'),
            E(1, 'e', 'AR'),
            E(0, 'b', 'R'),
            E(1, 'a', 'R'),
        )
        tree = SimpleTreeReader().readfile(
            self._test_path(self.SHARED_TEST_DIR, 'tree19.stree')
        )

        self.assertEqual(
            tuple(PreOrderedXBWTransform.from_tree(tree)), expected
        )

    def test_ferragina_tree(self):
        expected = (
            E(0, 'A', ''),
            E(0, 'B', 'A'),
            E(0, 'D', 'BA'),
            E(1, 'a', 'DBA'),
            E(0, 'a', 'BA'),
            E(1, 'E', 'BA'),
            E(1, 'b', 'EBA'),
            E(0, 'C', 'A'),
            E(0, 'D', 'CA'),
            E(1, 'c', 'DCA'),
            E(0, 'b', 'CA'),
            E(1, 'D', 'CA'),
            E(1, 'c', 'DCA'),
            E(1, 'B', 'A'),
            E(1, 'D', 'BA'),
            E(1, 'b', 'DBA'),
        )
        tree = SimpleTreeReader().readfile(
            self._test_path(self.SHARED_TEST_DIR, 'tree18-ferragina.stree')
        )

        self.assertEqual(
            tuple(PreOrderedXBWTransform.from_tree(tree)), expected
        )


if __name__ == '__main__':
    unittest.main()
