#!/usr/bin/env python3
import argparse

from parser_commands.wrap_command import wrap_command
from parser_commands.xbw.CompressXBWCommand import CompressXBWCommand
from parser_commands.xbw.DecompressXBWCommand import DecompressXBWCommand
from parser_commands.xbw.PrintXBWCommand import PrintXBWCommand


def _build_parser():
    parser = argparse.ArgumentParser(
        description='Collection of xbw utilities'
    )
    subparsers = parser.add_subparsers(
        dest='subcommand', required=True
    )
    commands = [
        PrintXBWCommand,
        # CompressXBWCommand,
        # DecompressXBWCommand,
    ]

    for c in commands:
        p = c.set_up_parser(
            subparsers.add_parser(
                c.COMMAND_NAME,
                description=c.DESCRIPTION
            )
        )
        p.set_defaults(func=wrap_command(c))

    return parser


def main():
    parser = _build_parser()
    args = parser.parse_args()

    args.func(args)

    return 0


if __name__ == "__main__":
    exit(main())
