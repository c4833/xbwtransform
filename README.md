# XBW Transform
A bachelor student's thesis code to test the XBW transform on labeled trees, as discussed in the 2009 academic article
[Compressing and indexing labeled trees, with applications][Ferragina2009], and perform string-compression experiments.

## Simple Tree Format
Files with the `.stree` format contain Simple Tree representations. (A Simple Tree is a term coined within this
repository.) Simple Tree files are able to express labeled trees in a simple yet effective way. One example could be

```
A
    B
    C
        c
    D
        a
        b
        F
            f
            a
```

To express a Simple Tree, you are **forced** to indent with tabs `\t`. While alternatives do exist--like
Graphviz [Dot language][Dot]--they were more tiresome to use than the Simple Tree format.

## Scripts
To access this repository utilities, you run one of the provided scripts, each dedicated to their specific task.

### `xbw.py`
The `xbw.py` script is a collection of subcommands to interact with the XBW transform.
Let `cmd` be a subcommand, and `args` the command-line arguments you want to feed to it. Then to
invoke `cmd` with `args`

```sh
python3 -m xbw.py cmd args
```

To get some help in using this script, and obtain the total list of subcommands

```sh
python3 -m xbw.py --help
```

### `lz.py`
The `lz.py` script is a collection of utilities related to the LZ family of compression algorithms, helping
to study the *logarithmic compression* of a string.
You run it and get help for it in the same way as for the `xbw.py` script.


### `bwt.py`
The `bwt.py` script allows to run the Burrows-Wheeler Transform encoding and decoding algorithm on a string.

## Unit Tests

A modest unit test suite is available. To run it

```sh
python3 -m unittest discover -p "*TestCase.py" -v
```

## PyCharm

Run configurations for scripts and the test suite are available for the PyCharm IDE.

[Ferragina2009]: https://dl.acm.org/doi/10.1145/1613676.1613680 "Compressing and indexing labeled trees, with applications"
[Dot]: https://www.graphviz.org/doc/info/lang.html "Dot Language"