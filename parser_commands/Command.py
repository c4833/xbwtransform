class Command:
    def __init__(self, name, description):
        self._name = str(name)
        self._description = str(description)

    @property
    def name(self):
        return self._name

    @property
    def description(self):
        return self._description

    def set_up_parser(self, parser):
        raise NotImplemented()
