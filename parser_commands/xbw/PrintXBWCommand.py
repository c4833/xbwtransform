from tabularprint import tabularprint

from core.tree.SimpleTreeReader import SimpleTreeReader
from core.xbw.OrderedXBWTransform import OrderedXBWTransform
from core.xbw.PreOrderedXBWTransform import PreOrderedXBWTransform


class PrintXBWCommand:
    COMMAND_NAME = 'printxbw'
    DESCRIPTION = 'Print out the xbw transform of a tree'

    def __init__(self, args):
        self._tree = SimpleTreeReader().readfile(args.tree_path[0])
        self._args = args

        if args.no_sort:
            self._transform = PreOrderedXBWTransform.from_tree(self._tree)
        else:
            self._transform = OrderedXBWTransform.from_tree(self._tree)

    @staticmethod
    def set_up_parser(parser):
        parser.add_argument(
            'tree_path', type=str, nargs=1,
            help='Simple tree file path'
        )
        parser.add_argument(
            '--no-sort', default=False, action='store_const', const=True,
            help='If specified, the stable sort step is NOT performed during '
                 'the xbw transform'
        )
        parser.add_argument(
            '--csv', default=False, action='store_const', const=True,
            help='If true, the output is given in CSV form'
        )

        return parser

    def __call__(self):
        if self._args.csv:
            self._print_csv()
        else:
            self._print_table()

    def _print_csv(self):
        print('is last,label,context')

        for entry in self._transform:
            print(int(entry.last), entry.label, entry.suffix, sep=',')

    def _print_table(self):
        tabularprint.table(
            ('Is last?', 'Label', 'Context'),
            tuple(
                map(
                    lambda e: (int(e.last), e.label, e.suffix),
                    self._transform
                )
            )
        )
