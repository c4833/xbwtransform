import argparse
import bz2
import gzip
import zlib

from tabularprint import tabularprint

from core.xbw.OrderedXBWTransform import OrderedXBWTransform


class DecompressXBWCommand:
    COMMAND_NAME = 'decompress'
    DESCRIPTION = 'Decompress the xbw transform of a tree'

    AVAILABLE_DECOMPRESSORS = {
        'gzip': (gzip.decompress, gzip.decompress),
        'zlib': (zlib.decompress, zlib.decompress),
        'bz2': (bz2.decompress, bz2.decompress),
    }

    def __init__(self, args):
        self._args = args
        self._transform = OrderedXBWTransform.decompress(
            self._args.path[0].read(),
            *self.AVAILABLE_DECOMPRESSORS[self._args.decompressor]
        )

    @classmethod
    def set_up_parser(cls, parser):
        decompressors = tuple(cls.AVAILABLE_DECOMPRESSORS.keys())

        parser.add_argument(
            'decompressor', choices=decompressors,
            help='Decompressor to use'
        )
        parser.add_argument(
            'path', type=argparse.FileType('rb'), nargs=1,
            help='Input file path. The referenced file must contain the '
                 'compressed representation of an xbw transform'
        )
        parser.add_argument(
            '-t', '--to-tree', action='store_true',
            help='Whether to output the transform in a tree format'
        )
        parser.add_argument(
            '--csv', default=False, action='store_const', const=True,
            help='This is incompatible with the --to-tree flag. If true, '
                 'the output is given in CSV format'
        )

        return parser

    def __call__(self):
        if self._args.to_tree:
            self._print_tree()
        else:
            self._print_transform()

    def _print_tree(self):
        print(str(self._transform.to_tree()))

    def _print_transform(self):
        if self._args.csv:
            print('is last,label,context')

            for entry in self._transform:
                print(
                    str(int(entry.last)), entry.label, entry.suffix,
                    sep=','
                )
        else:
            tabularprint.table(
                ('Is last?', 'Label', 'Context'),
                tuple(
                    map(
                        lambda e: (int(e.last), e.label, e.suffix),
                        self._transform
                    )
                ),
            )
