import argparse
import bz2
import gzip
import zlib

from tabularprint import table

from core.tree.SimpleTreeReader import SimpleTreeReader
from core.xbw.OrderedXBWTransform import OrderedXBWTransform


class CompressXBWCommand:
    COMMAND_NAME = 'compress'
    DESCRIPTION = 'Compress the xbw transform of a tree'

    AVAILABLE_COMPRESSORS = {
        'gzip': (gzip.compress, gzip.compress),
        'zlib': (zlib.compress, zlib.compress),
        'bz2': (bz2.compress, bz2.compress),
    }

    def __init__(self, args):
        self._args = args
        self._tree = SimpleTreeReader().readfile(args.tree_path[0])
        self._transform = OrderedXBWTransform.from_tree(self._tree)
        self._compressed_transform = self._transform.compress(
            *self.AVAILABLE_COMPRESSORS[args.compressor]
        )

    @classmethod
    def set_up_parser(cls, parser):
        compressors = tuple(cls.AVAILABLE_COMPRESSORS.keys())

        parser.add_argument(
            'tree_path', type=str, nargs=1,
            help='Input file location. This must be in the Simple tree format'
        )
        parser.add_argument(
            'output', type=argparse.FileType('wb'),
            help='Output file location'
        )
        parser.add_argument(
            '--stats', action='store_true',
            help='Print out statistical information, like the percentage of '
                 'saved space'
        )
        parser.add_argument(
            '--compressor', choices=compressors, default=compressors[0],
            help='Compressor to use'
        )

        return parser

    def __call__(self):
        self._args.output.write(self._compressed_transform)

        if self._args.stats:
            self._print_stats()

    def _print_stats(self):
        uncompressed_length = len(
            (self._transform.lasts + self._transform.labels).encode()
        )
        compressed_length = len(self._compressed_transform)

        # TODO Set up dependency injection for Python packages like tabularprint
        table(
            ('Parameter', 'Value'),
            (
                ('Uncompressed length (bytes)', uncompressed_length),
                ('Compressed length (bytes)', compressed_length),
            )
        )
