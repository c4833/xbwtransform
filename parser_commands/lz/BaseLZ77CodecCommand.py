import argparse

from core.compression.LZ77Codec import LZ77Codec


class BaseLZ77CodecCommand:
    @staticmethod
    def set_up_parser(parser):
        defaults = LZ77Codec.__init__.__kwdefaults__

        parser.add_argument(
            'input', type=argparse.FileType('r'),
            help='A file to read from. The - flag specifies standard input.'
        )
        parser.add_argument(
            '--buffer', '-b', type=int, default=defaults['buffer'],
            help='Size (in characters) of the internal buffer'
        )
        parser.add_argument(
            '--look-ahead', '-l', type=int, default=defaults['look_ahead'],
            help='Size (in characters) of the look-ahead window'
        )

        return parser
