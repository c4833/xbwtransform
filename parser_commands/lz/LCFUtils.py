import re


class LCFUtils:
    LCF_PATTERN = re.compile(r'lcf-(\d+)\.lz77')

    @classmethod
    def format(cls, index):
        return 'lcf-%d.lz77' % index

    @classmethod
    def is_lcf_name(cls, name):
        return bool(
            re.fullmatch(cls.LCF_PATTERN, name)
        )

    @classmethod
    def lcf_index(cls, name):
        match = re.fullmatch(cls.LCF_PATTERN, name)

        return int(match.group(1))
