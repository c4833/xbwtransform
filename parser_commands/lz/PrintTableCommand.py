from math import floor, log, ceil

from tabularprint import tabularprint

from decorators.to import to
from parser_commands.lz.BaseLZ77StatsCommand import BaseLZ77StatsCommand
from parser_commands.lz.LZ77Parameters import LZ77Parameters
from utils import parse_range


class PrintTableCommand(BaseLZ77StatsCommand):
    COMMAND_NAME = 'print_table'
    DESCRIPTION = 'Outputs a table with some data describing the feasibility ' \
                  'of obtaining logarithmic compression with the LZ77 ' \
                  'algorithm'

    def __init__(self, args):
        super().__init__()

        self._args = args
        self._params = LZ77Parameters(
            args.buffer, args.lookahead_buffer, args.symbol_size,
            args.log_factor
        )

    @staticmethod
    def set_up_parser(parser):
        parser = BaseLZ77StatsCommand.set_up_parser(parser)

        parser.add_argument(
            'exponents', type=parse_range,
            help='Exponents to the base of 2 upon which the table is to be '
                 'constructed. This is expressed in the start:end:step format'
        )
        parser.add_argument(
            '-m', '--symbol-size', type=int, default=8,
            help='Number of bits needed to represent a symbol of the input '
                 'string'
        )
        parser.add_argument(
            '--format', choices=('console', 'latex'), default='console',
            help='Output format'
        )
        parser.add_argument(
            '-v', '--verbose', action='store_true',
            help='Display additional information'
        )

        return parser

    def __call__(self):
        format_function = {
            'console': self._print_table_console,
            'latex': self._print_table_latex
        }

        if self._args.verbose:
            self._print_verbose()

        format_function[self._args.format](self._build_table())

        return 0

    @to(tuple)
    def _build_table(self):
        for e in self._args.exponents:
            yield TableEntry(e, self._params)

    @staticmethod
    def _print_table_console(table):
        tabularprint.table(
            (
                'input length', 'max. chunks num.', 'chunks length (symbols)',
                'output size %'
            ),
            tuple(
                map(
                    lambda e: (
                        '2 ^ %d' % e.exponent,
                        e.chunks,
                        e.chunk_length,
                        # e.input_size,
                        # e.chunks_size * e.chunks,
                        '%.2f%%' % e.output_size_percent
                    ),
                    table
                )
            )
        )

    @staticmethod
    def _print_table_latex(table):
        for index, entry in enumerate(table):
            print(
                '$2^{%d}$' % entry.exponent,
                '$%d$' % entry.chunks,
                '$%d$' % entry.chunk_length,
                '$%.2f$' % entry.output_size_percent,
                sep=' & ',
                end=''
            )

            if index < len(table) - 1:
                print(' \\\\')
            else:
                print()

    def _print_verbose(self):
        print('Bits per symbol: %d' % self._params.symbol_size)
        print('Bits per encoded block: %d' % self._params.block_size)
        print()


class TableEntry:
    def __init__(self, exponent, lz77_params, base=2):
        self._exp = int(exponent)
        self._base = int(base)
        self._params = lz77_params

    @property
    def exponent(self):
        return self._exp

    @property
    def input_length(self):
        return pow(self._base, self._exp)

    @property
    def input_size(self):
        return self._params.symbol_size * self.input_length

    @property
    def chunks(self):
        # Although the formula is c / d * log(m n), since we assume c to be
        #  a multiple of d, we omit the division by d below. log_factor is equal
        #  to c in my equations
        return floor(self._params.log_factor * log(self.input_size, self._base))

    @property
    def chunk_length(self):
        return ceil(self.input_length / self.chunks)

    @property
    def output_size(self):
        return self._params.block_size * self.chunks

    @property
    def output_size_percent(self):
        return 100 * (self.output_size / self.input_size)
