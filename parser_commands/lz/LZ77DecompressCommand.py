from core.compression.LZ77Codec import LZ77Codec
from parser_commands.lz.BaseLZ77CodecCommand import BaseLZ77CodecCommand


class LZ77DecompressCommand(BaseLZ77CodecCommand):
    COMMAND_NAME = 'decompress'
    DESCRIPTION = 'Decompress a previously-compressed string using the LZ1 ' \
                  'algorithm'

    def __init__(self, args):
        super().__init__()

        self._args = args

    def __call__(self):
        codec = LZ77Codec(
            buffer=self._args.buffer, look_ahead=self._args.look_ahead
        )
        print(codec.decompress(self._args.input.read().strip()))
