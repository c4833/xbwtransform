import os
import random
import string
from math import ceil, log

from parser_commands.lz.BaseLZ77StatsCommand import BaseLZ77StatsCommand
from parser_commands.lz.LCFUtils import LCFUtils
from parser_commands.lz.LZ77Parameters import LZ77Parameters
from utils import parse_range


class WriteLCFsCommand(BaseLZ77StatsCommand):
    COMMAND_NAME = 'write_lcfs'
    DESCRIPTION = 'Given a series of target lengths, creates corresponding ' \
                  'LCFs (Logarithmically Compressible Files) of the given ' \
                  'number of symbols. It is assumed that each input symbol ' \
                  'is one byte long'
    SEPARATOR = ' '

    def __init__(self, args):
        super().__init__()

        self._args = args
        self._alphabet = string.ascii_letters
        self._params = LZ77Parameters(
            args.buffer,
            args.lookahead_buffer,
            ceil(log(len(self._alphabet), 2)),
            args.log_factor
        )
        self._base = 2

    @staticmethod
    def set_up_parser(parser):
        parser = BaseLZ77StatsCommand.set_up_parser(parser)

        parser.add_argument(
            'exponents', type=parse_range,
            help='Exponents to the base of 2. These indicate the length of the '
            'LCFs (Logarithmically Compressible Files) which are to be '
            'produced, and are provided in the start:end:step format'
        )
        parser.add_argument(
            'output_dir', type=str,
            help='Directory location where to write LCFs'
        )
        parser.add_argument(
            '-c', '--chunks', type=int, default=20,
            help='Number of distinct chunks'
        )
        parser.add_argument(
            '-cw', '--chunks-per-window', type=int, default=10,
            help='Maximum number of equal-sized chunks that can be held in the '
                 'window buffer'
        )
        parser.add_argument(
            '--debug', action='store_true', default=False,
            help='Useful for debug and unit test purposes. If specified, the '
                 'output chunks are separated by a separator character'
        )

        return parser

    def __call__(self):
        for e in self._args.exponents:
            with open(
                    os.path.join(self._args.output_dir, LCFUtils.format(e)), 'w'
            ) as lcf_file:
                self._write_lcf(lcf_file, self._base ** e)

    def _write_lcf(self, file, size):
        chunks = tuple(self._build_chunks())
        separator = self.SEPARATOR if self._args.debug else ''

        while size > 0:
            chunk = random.choice(chunks)
            is_last = size <= len(chunk)

            file.write(chunk)

            if not is_last:
                file.write(separator)
                size -= len(separator)

            size -= len(chunk)

    def _build_chunks(self):
        chunk = self._chunk_generator(
            self._params.window_size // self._args.chunks_per_window
        )

        for _ in range(self._args.chunks):
            yield next(chunk)

    def _chunk_generator(self, chunk_length):
        random.seed()

        while True:
            character = random.choice(self._alphabet)

            yield chunk_length * character
