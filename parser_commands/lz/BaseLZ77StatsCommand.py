class BaseLZ77StatsCommand:
    @staticmethod
    def set_up_parser(parser):
        parser.add_argument(
            '-f', '--log-factor', type=float, default=10,
            help='Factor to be applied to the log function, expressed as a '
                 'multiple of the block size'
        )
        parser.add_argument(
            '-b', '--buffer', type=int, default=2 ** 10,
            help='Buffer size of the LZ77 encoder'
        )
        parser.add_argument(
            '-l', '--lookahead-buffer', type=int, default=2 ** 9,
            help='Lookahead buffer size of the LZ77 encoder'
        )

        return parser
