import math
import os
import zlib
from collections import namedtuple

from tabularprint import tabularprint

from core.compression.LZ77Codec import LZ77Codec
from parser_commands.lz.LCFUtils import LCFUtils

TableEntry = namedtuple(
    'TableEntry',
    ('file_name', 'index', 'original_bytes', 'compressed_bytes', 'log')
)


class GZIPCompressLCFsCommand:
    COMMAND_NAME = 'analyze_lcfs_gzip'
    DESCRIPTION = 'Analyze the compression of LCFs (Logarithmically ' \
                  'Compressible Files) using GZIP.'

    def __init__(self, args):
        self._args = args

    @staticmethod
    def set_up_parser(parser):
        parser.add_argument(
            'directory', type=str,
            help='Directory where to look for LCFs (Logarithmically '
                 'Compressible Files)'
        )

        return parser

    def __call__(self):
        tabularprint.table(
            ('file name', 'index', 'bytes', 'log2(bytes)', 'compressed bytes'),
            sorted(
                self._build_entries(),
                key=lambda x: x.index
            )
        )

    def _build_entries(self):
        for file_name in os.listdir(self._args.directory):
            if LCFUtils.is_lcf_name(file_name):
                yield self._build_entry(file_name)

    def _build_entry(self, file_name):
        file_path = os.path.join(self._args.directory, file_name)

        with open(file_path, 'br') as lcf_file:
            # Note: the code below is highly inefficient (but this should not
            #  be a reason of concern for the data sizes we consider)
            data = lcf_file.read(-1)
            compressed_data = zlib.compress(data, level=9)

            # TODO Remove
            assert(data == zlib.decompress(compressed_data))

            # ('file_name', 'index', 'original_bytes', 'com_bytes', 'log')
            return TableEntry(
                file_name,
                LCFUtils.lcf_index(file_name),
                len(data),
                math.ceil(
                    math.log2(len(data))
                ),
                len(compressed_data),
            )
