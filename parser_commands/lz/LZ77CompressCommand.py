from core.compression.LZ77Codec import LZ77Codec
from parser_commands.lz.BaseLZ77CodecCommand import BaseLZ77CodecCommand


class LZ77CompressCommand(BaseLZ77CodecCommand):
    COMMAND_NAME = 'compress'
    DESCRIPTION = 'Compress an alphanumeric string using the LZ1 algorithm'

    def __init__(self, args):
        super().__init__()

        self._args = args

    @staticmethod
    def set_up_parser(parser):
        parser = BaseLZ77CodecCommand.set_up_parser(parser)

        parser.add_argument(
            '--sep', '-s', type=str, default='',
            help='Separator string between encoded words (useful for debugging)'
        )

        return parser

    def __call__(self):
        codec = LZ77Codec(
            buffer=self._args.buffer, look_ahead=self._args.look_ahead
        )
        print(
            *codec.compress(self._args.input.read()), sep=self._args.sep, end=''
        )
