import os
from collections import namedtuple

from tabularprint import tabularprint

from core.compression.LZ77Codec import LZ77Codec
from parser_commands.lz.LCFUtils import LCFUtils

TableEntry = namedtuple(
    'TableEntry', ('file_name', 'index', 'blocks_num')
)


class CompressLCFsCommand:
    COMMAND_NAME = 'analyze_lcfs'
    DESCRIPTION = 'Analyze the compression of LCFs (Logarithmically ' \
                  'Compressible Files), reporting the number of ' \
                  'blocks into which an LCF is split by the LZ77 algorithm.'

    def __init__(self, args):
        self._args = args
        self._codec = LZ77Codec(buffer=args.buffer, look_ahead=args.look_ahead)

    @staticmethod
    def set_up_parser(parser):
        defaults = LZ77Codec.__init__.__kwdefaults__

        parser.add_argument(
            'directory', type=str,
            help='Directory where to look for LCFs (Logarithmically '
                 'Compressible Files)'
        )
        parser.add_argument(
            '-b', '--buffer', type=int, default=defaults['buffer'],
            help='Size (in characters) of the internal buffer'
        )
        parser.add_argument(
            '-l', '--look-ahead', type=int, default=defaults['look_ahead'],
            help='Size (in characters) of the look-ahead window'
        )

        return parser

    def __call__(self):
        tabularprint.table(
            ('file name', 'index', 'blocks num.'),
            sorted(
                self._build_entries(),
                key=lambda x: x.index
            )
        )

    def _build_entries(self):
        for file_name in os.listdir(self._args.directory):
            if LCFUtils.is_lcf_name(file_name):
                yield self._build_entry(file_name)

    def _build_entry(self, file_name):
        file_path = os.path.join(self._args.directory, file_name)

        with open(file_path) as lcf_file:
            blocks = len(
                tuple(
                    self._codec.compress(lcf_file.read())
                )
            )

            return TableEntry(file_name, LCFUtils.lcf_index(file_name), blocks)
