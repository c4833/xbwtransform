from dataclasses import dataclass
from math import ceil, log


@dataclass
class LZ77Parameters:
    buffer: int
    lookahead_buffer: int
    # Number of bits per symbol
    symbol_size: int
    log_factor: float
    base: float = 2

    @property
    def block_size(self):
        return self.symbol_size + \
               ceil(log(self.buffer - self.lookahead_buffer, self.base)) + \
               ceil(log(self.lookahead_buffer, self.base))

    @property
    def window_size(self):
        """
        :return: The window size, also known as the "look-behind" buffer length.
        """
        return self.buffer - self.lookahead_buffer
