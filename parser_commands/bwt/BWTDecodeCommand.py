import burrowswheeler

from parser_commands.bwt.BaseBWTCommand import BaseBWTCommand


class BWTDecodeCommand(BaseBWTCommand):
    COMMAND_NAME = 'decode'
    DESCRIPTION = 'Decode a string previously encoded by the BWT ' \
                  '(Burrows-Wheeler Transform)'

    def __init__(self, args):
        self._args = args

    def __call__(self):
        self._args.output.write(
            burrowswheeler.inverse(
                self._args.input.read()
            )
        )
