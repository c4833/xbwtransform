from collections import namedtuple

import burrowswheeler
from tabularprint import tabularprint

from utils import parse_range
from utils.rle import rle

TableEntry = namedtuple(
    'TableEntry', ('original_size', 'compressed_size')
)


class PlotBWTRLECommand:
    COMMAND_NAME = 'plot_bwt_rle'
    DESCRIPTION = 'Runs the BWT and RLE on a series of length-increasing ' \
                  'and reports the results in a textual format.'

    def __init__(self, args):
        self._args = args

    @classmethod
    def set_up_parser(cls, parser):
        parser.add_argument(
            'exponents', type=parse_range,
            help='The length of the input strings to the compression '
                 'scheme is expressed as 2 to the power of an integer. This '
                 'parameter allows to express how many exponents are to be '
                 'fed to the algorithm in range notation'
        )
        parser.add_argument(
            '-s', '--sample_string', type=str, default='ab',
            help='Sample string to concatenate to itself a number of times '
                 'determined by the exponents parameter.'
        )
        parser.add_argument(
            '-f', '--format', choices=('table', 'csv'), default='table',
            help='The format in which to display the results'
        )
        return parser

    def __call__(self):
        display_function = {
            'table': self._print_tabular,
            'csv': self._print_csv
        }

        display_function[self._args.format](
            self._build_table(self._args.exponents)
        )

    def _build_table(self, exponents):
        for e in exponents:
            length = 2 ** e
            factor = int(length / len(self._args.sample_string))
            to_compress = factor * self._args.sample_string
            compressed = ''.join(
                rle(burrowswheeler.transform(to_compress))
            )

            yield TableEntry(len(to_compress), len(compressed))

    @staticmethod
    def _print_tabular(results):
        tabularprint.table(
            ('Original size', 'Compressed size'),
            tuple(results)
        )

    @staticmethod
    def _print_csv(results):
        for r in results:
            print(*r, sep=',')
