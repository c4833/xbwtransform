import argparse
import sys


class BaseBWTCommand:
    @classmethod
    def set_up_parser(cls, parser):
        parser.add_argument(
            'input', type=argparse.FileType('r'),
            help='A file to read from. The - flag specifies standard input.'
        )
        parser.add_argument(
            'output', nargs='?', type=argparse.FileType('w'),
            default=sys.stdout,
            help='A file to write to. Defaults to standard output.'
        )

        return parser
