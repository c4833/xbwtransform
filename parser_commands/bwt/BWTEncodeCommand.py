import burrowswheeler

from parser_commands.bwt.BaseBWTCommand import BaseBWTCommand


class BWTEncodeCommand(BaseBWTCommand):
    COMMAND_NAME = 'encode'
    DESCRIPTION = 'Encode a string according to the BWT (Burrows-Wheeler ' \
                  'Transform)'

    def __init__(self, args):
        self._args = args

    def __call__(self):
        self._args.output.write(
            burrowswheeler.transform(
                self._args.input.read()
            )
        )
