from core.xbw.BaseXBWTransform import BaseXBWTransform
from core.xbw.XBWTransformVisit import XBWTransformVisit


class PreOrderedXBWTransform(BaseXBWTransform):
    @classmethod
    def from_tree(cls, tree):
        visit = XBWTransformVisit()

        return cls(visit(tree))
