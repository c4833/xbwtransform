from core.xbw.spine.SpineAutomaton import SpineAutomaton


def spines(automaton: SpineAutomaton):
    while automaton.has_next():
        automaton.next()

        if automaton.state == automaton.State.ACCEPTED:
            yield automaton.spine_range()

            automaton.reset_state()
