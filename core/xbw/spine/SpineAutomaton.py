from core.xbw.PreOrderedXBWTransform import PreOrderedXBWTransform


class SpineAutomaton:
    def __init__(self, xbw_transform: PreOrderedXBWTransform):
        self._transform = xbw_transform
        self._index, self._length = -1, len(self._transform)
        self._state = None

        self.reset_state()

    @property
    def state(self):
        return self._state

    def reset_state(self):
        pass

    def has_next(self):
        return self._index < self._length

    def next(self):
        if not self.has_next():
            raise OverflowError('Nothing more to iterate on')

        # Observe that BEFORE the first (zeroth) iteration self._index == -1
        self._index += 1
        self._process_state()

    def _process_state(self):
        raise NotImplemented()

    def _is_leaf(self, index):
        if index == self._length - 1:
            return True
        elif 0 < index < self._length - 1:
            # Only for readability
            curr, _next = self._transform[index], self._transform[index + 1]

            return len(_next.suffix) <= len(curr.suffix)
        elif self._length <= index:
            raise OverflowError()
        else:
            return False
