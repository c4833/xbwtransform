import enum

from core.xbw.PreOrderedXBWTransform import PreOrderedXBWTransform
from core.xbw.spine.SpineAutomaton import SpineAutomaton


class RightSpineAutomaton(SpineAutomaton):
    class State(enum.Enum):
        RESET = enum.auto()
        LEFT = enum.auto()
        RIGHT = enum.auto()
        ACCEPTED = enum.auto()

    def __init__(self, xbw_transform: PreOrderedXBWTransform):
        super().__init__(xbw_transform)

        self._start = None

    def reset_state(self):
        self._state = self.State.RESET
        self._start = None

    def spine_range(self):
        if self._state == self.State.ACCEPTED:
            return self._start, self._end

        return None

    def _process_state(self):
        state_to_method = {
            self.State.RESET: self._process_reset,
            self.State.LEFT: self._process_left,
            self.State.RIGHT: self._process_right,
            self.State.ACCEPTED: self._process_accept
        }

        state_to_method[self._state]()

    def _process_reset(self):
        if 0 < self._index < self._length:
            i = self._index
            old, curr = self._transform[i - 1], self._transform[i]

            if self._is_leaf(i) and curr.suffix == old.label + old.suffix and not curr.last:
                self._start = i - 1
                self._state = self.State.LEFT

    def _process_left(self):
        i = self._index
        old, curr = self._transform[i - 1], self._transform[i]

        if i < self._length and curr.last and curr.suffix == old.suffix:
            self._state = self.State.RIGHT
        else:
            self.reset_state()

    def _process_right(self):
        i = self._index

        if 0 < i < self._length:
            old, curr = self._transform[i - 1], self._transform[i]

            if not curr.last and curr.suffix == old.label + old.suffix and self._is_leaf(i):
                self._state = self.State.LEFT
            elif self._is_leaf(i - 1):
                self._end = i - 1
                self._state = self.State.ACCEPTED
            else:
                self.reset_state()
        else:
            self._end = i - 1
            self._state = self.State.ACCEPTED

    def _process_accept(self):
        pass

