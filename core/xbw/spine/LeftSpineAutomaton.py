import enum

from core.xbw.spine.SpineAutomaton import SpineAutomaton
from core.xbw.PreOrderedXBWTransform import PreOrderedXBWTransform


class LeftSpineAutomaton(SpineAutomaton):
    class State(enum.Enum):
        RESET = enum.auto()
        UP = enum.auto()
        DOWN = enum.auto()
        ACCEPTED = enum.auto()

    def __init__(self, xbw_transform: PreOrderedXBWTransform):
        super().__init__(xbw_transform)
        self._height, self._opposite = None, None

    def reset_state(self):
        self._height = None
        self._opposite = None
        self._state = self.State.RESET

    def spine_range(self):
        if self._state == self.State.ACCEPTED:
            return self._opposite, self._index - 1

        return None

    def _process_state(self):
        state_to_method = {
            self.State.RESET: self._process_reset,
            self.State.UP: self._process_up,
            self.State.DOWN: self._process_down,
            self.State.ACCEPTED: self._process_accept
        }

        state_to_method[self._state]()

    def _process_reset(self):
        if self._index == 0:
            self._height = 1
            self._state = self.State.UP
        elif 0 < self._index < self._length:
            old, curr = self._transform[self._index - 1], self._transform[self._index]

            if not curr.last and curr.suffix == old.label + old.suffix:
                self._height = 1
                self._state = self.State.UP
        else:
            # Redundant
            # self._state = self.State.RESET
            self.reset_state()

    def _process_up(self):
        if 0 < self._index < self._length:
            i = self._index  # Shortcut
            old, curr = self._transform[i - 1], self._transform[i]

            if not curr.last and curr.suffix == old.label + old.suffix:
                # Commented because redundant
                # self._state = self.State.UP
                self._height += 1
            elif self._is_leaf(i - 1) and self._is_leaf(i) and not old.last and curr.last and curr.suffix == old.suffix:
                self._opposite = i - 2
                self._height -= 1
                self._state = self.State.DOWN
            else:
                self.reset_state()

    def _process_down(self):
        # Shortcuts
        t, i, o = self._transform, self._index, self._opposite

        if 0 < self._height and i < self._length and self._is_leaf(i) and not t[o].last and t[i].last and \
                t[i].suffix == t[o].suffix:
            # self._state = self.State.DOWN
            self._opposite -= 1
            self._height -= 1
        else:
            self._state = self.State.ACCEPTED

    def _process_accept(self):
        pass

