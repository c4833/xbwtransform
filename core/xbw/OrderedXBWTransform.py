from core.tree.Tree import Tree
from core.xbw.BaseXBWTransform import BaseXBWTransform
from core.xbw.BytesPacker import BytesPacker
from core.xbw.codec.XBWTransformCodecBundle import \
    XBWTransformCodecBundle
from core.xbw.BaseXBWEntry import BaseXBWEntry
from core.xbw.XBWTransformVisit import XBWTransformVisit


class OrderedXBWTransform(BaseXBWTransform):
    def __init__(self, entries):
        # TODO Ensure core methods like _build_f, _build_j and
        #  _rebuild_transform are correct
        super().__init__(sorted(entries))

        self._alphabet = self._build_internal_alphabet()

    def _build_internal_alphabet(self):
        alphabet = []

        for entry in self[1:]:
            label = entry.suffix[0] if entry.suffix else None

            if label and (not alphabet or label != alphabet[-1]):
                alphabet.append(label)

        return alphabet

    @staticmethod
    def _build_f(transform, alphabet):
        # TODO Can the alphabet parameter be avoided?
        f = dict()

        if alphabet:
            count = dict()

            for entry in transform:
                count[entry.label] = count.get(entry.label, 0) + 1

            f[alphabet[0]] = 1

            for index, label in enumerate(alphabet[:-1]):
                lasts = 0
                next_index = f[label]

                while lasts != count[label]:
                    if transform[next_index].last:
                        lasts += 1

                    next_index += 1

                f[alphabet[index + 1]] = next_index

        return f

    @staticmethod
    def _build_j(transform, f):
        j = dict()

        for index, entry in enumerate(transform):
            if entry.is_leaf:
                j[index] = -1
            else:
                z = j[index] = f[entry.label]

                while z < len(transform) and not transform[z].last:
                    z += 1

                f[entry.label] = z + 1

        return j

    @classmethod
    def _rebuild_transform(cls, lasts, labels, alphabet):
        def build_entry(pair):
            return BaseXBWEntry(int(pair[0]), pair[1], '')

        # A partial transform, whose suffixes haven't been reconstructed yet
        t = tuple(
            map(build_entry, zip(lasts, labels))
        )
        j = cls._build_j(t, cls._build_f(t, alphabet))
        queue = [0]

        while queue:
            parent = queue.pop(0)
            # First child index, which is initially equal to the last child one
            last_child = first_child = j[parent]

            if first_child > 0:
                # Compute the actual value of last_child
                while last_child < len(t) - 1 and not t[last_child].last:
                    last_child += 1

                for child in range(first_child, last_child + 1):
                    t[child].suffix = t[parent].label + t[parent].suffix
                    queue.append(child)

        return OrderedXBWTransform(t)

    @staticmethod
    def from_tree(tree):
        visit = XBWTransformVisit()

        return OrderedXBWTransform(sorted(visit(tree)))

    def to_tree(self):
        j = self._build_j(self, self._build_f(self, self._alphabet))
        root = Tree(self[0].label)
        queue = [(root, 0)]
        length = len(self)

        while queue:
            node, index = queue.pop(0)
            last_child = first_child = j[index]

            if first_child > 0:
                while last_child < length - 1 and not self[last_child].last:
                    last_child += 1

                for child in range(first_child, last_child + 1):
                    child = Tree(self[first_child].label)

                    node.append_child(child)
                    queue.append((child, first_child))
                    first_child += 1

        return root

    def compress(self, codec: XBWTransformCodecBundle):
        p = BytesPacker()

        return p.pack(
            codec.last_compressor(self.lasts.encode()),
            codec.label_compressor(self.labels.encode()),
            ''.join(self._alphabet).encode()
        )

    @classmethod
    def decompress(cls, compressed_xbw, codec: XBWTransformCodecBundle):
        p = BytesPacker()
        lasts, labels, alphabet = p.unpack(compressed_xbw)

        return cls._rebuild_transform(
            codec.last_decompressor(lasts).decode(),
            codec.label_decompressor(labels).decode(),
            alphabet.decode()
        )
