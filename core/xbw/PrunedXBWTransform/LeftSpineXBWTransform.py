from core.xbw.BytesPacker import BytesPacker
from core.xbw.PreOrderedXBWTransform import PreOrderedXBWTransform
from core.xbw.BaseXBWEntry import BaseXBWEntry


class LeftSpineXBWTransform(PreOrderedXBWTransform):
    def __init__(self, entries):
        if not self._has_left_pattern(entries):
            raise ValueError(
                'The provided entries do not follow a left spine pattern'
            )

        super().__init__(entries)

    @staticmethod
    def _has_left_pattern(entries):
        """
        :return: `True` if `entries` possesses a left-spine pattern, `False`
        otherwise.
        """
        height = (len(entries) - 1) // 2

        if len(entries) % 2 == 0:
            return False

        for i, e in enumerate(entries):
            if i == 0:
                pass
            elif 1 <= i <= height:
                prev = entries[i - 1]

                if e.last or e.suffix != prev.label + prev.suffix:
                    return False
            else:
                opposite = entries[len(entries) - i]

                if not e.last or e.suffix != opposite.suffix:
                    return False

        return True

    @property
    def height(self):
        return (len(self) - 1) // 2

    def compress(self, codec):
        p = BytesPacker()

        return p.pack(
            self[0].suffix.encode(),
            codec.label_compressor(self.labels.encode())
        )

    @classmethod
    def decompress(cls, compressed_xbw, codec):
        root_suffix, labels = BytesPacker().unpack(compressed_xbw)
        root_suffix = root_suffix.decode()
        labels = codec.label_decompressor(labels).decode()

        entries = [
            BaseXBWEntry(0, labels[0], root_suffix)
        ]
        height = (len(labels) - 1) // 2

        for i, label in enumerate(labels):
            if i == 0:
                pass
            elif i <= height:
                prev = entries[i - 1]

                entries.append(
                    BaseXBWEntry(0, label, prev.label + prev.suffix)
                )
            else:
                opposite = entries[len(labels) - i]

                entries.append(
                    BaseXBWEntry(1, label, opposite.suffix)
                )

        return cls(entries)
