from core.xbw.BytesPacker import BytesPacker
from core.xbw.PreOrderedXBWTransform import PreOrderedXBWTransform
from core.xbw.BaseXBWEntry import BaseXBWEntry


class RightSpineXBWTransform(PreOrderedXBWTransform):
    def __init__(self, entries):
        if not self._has_right_spine_pattern(entries):
            raise ValueError(
                'The provided entries do not follow a right spine pattern'
            )

        super().__init__(entries)

    @staticmethod
    def _has_right_spine_pattern(entries):
        if len(entries) % 2 == 0:
            return False

        for i, e in enumerate(entries):
            if i == 0:
                pass
            elif (i % 2) == 1:
                prev = entries[i - 1]

                if e.last or e.suffix != prev.label + prev.suffix:
                    return False
            else:
                prev = entries[i - 1]

                if not e.last or e.suffix != prev.suffix:
                    return False

        return True

    def compress(self, codec):
        return BytesPacker().pack(
            self[0].suffix.encode(),
            codec.label_compressor(
                ''.join(self.labels).encode()
            )
        )

    @classmethod
    def decompress(cls, compressed_xbw, codec):
        root_suffix, labels = BytesPacker().unpack(compressed_xbw)
        root_suffix, labels = root_suffix.decode(), \
                              codec.label_decompressor(labels).decode()

        entries = [
            BaseXBWEntry(0, labels[0], root_suffix)
        ]

        for i, label in enumerate(labels):
            if i == 0:
                pass
            elif i % 2 == 1:
                prev = entries[i - 1]

                entries.append(
                    BaseXBWEntry(0, label, prev.label + prev.suffix)
                )
            else:
                prev = entries[i - 1]

                entries.append(
                    BaseXBWEntry(1, label, prev.suffix)
                )

        return cls(entries)
