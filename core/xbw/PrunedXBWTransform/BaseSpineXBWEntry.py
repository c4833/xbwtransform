from core.xbw.BaseXBWEntry import BaseXBWEntry


class BaseSpineXBWEntry(BaseXBWEntry):
    def __init__(self, root, spine):
        super().__init__(
            root.last, root.label, root.suffix
        )
        self._spine = spine

    @property
    def spine(self):
        return self._spine
