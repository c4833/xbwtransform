from core.xbw.PrunedXBWTransform.BaseSpineXBWEntry import BaseSpineXBWEntry
from core.xbw.PrunedXBWTransform.RightSpineXBWTransform import \
    RightSpineXBWTransform


class RightSpineXBWEntry(BaseSpineXBWEntry):
    def __init__(self, entries):
        super().__init__(
            entries[0], RightSpineXBWTransform(entries)
        )
