from core.xbw.PrunedXBWTransform.BaseSpineXBWEntry import BaseSpineXBWEntry
from core.xbw.PrunedXBWTransform.LeftSpineXBWTransform import \
    LeftSpineXBWTransform


class LeftSpineXBWEntry(BaseSpineXBWEntry):
    def __init__(self, entries):
        super().__init__(
            entries[0], LeftSpineXBWTransform(entries)
        )
