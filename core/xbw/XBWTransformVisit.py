from functools import reduce
from operator import add

from core.tree.PreorderVisit import PreorderVisit
from core.xbw.BaseXBWEntry import BaseXBWEntry


class XBWTransformVisit(PreorderVisit):
    """
    A pre-order visit for computing the xbw transform of an ordered and labeled
    tree.
    """

    def __init__(self):
        PreorderVisit.__init__(self, "")

    @staticmethod
    def _is_last_child(tree):
        return tree.next_sibling is None and tree.parent is not None

    def _process_node(self, tree, suffix):
        # TODO Remove this check
        if tree.value.isupper() and not tree.first_child:
            raise ValueError(
                'Leaf nodes must be labeled with a lowercase character'
            )

        # TODO Add is leaf information
        return (
            BaseXBWEntry(self._is_last_child(tree), tree.value, suffix),
        )

    def _accumulate(self, suffix, tree):
        return str(tree.value) + suffix

    def _aggregate(self, root, children):
        # return root + children[0] + ... + children[n - 1]
        # where n = len(children)
        return reduce(
            add, (root, *children)
        )
