class CodecBundle:
    def __init__(self, *codecs):
        """
        :param codecs: An iterable of `(name, compressor, decompressor)` triples
        such that `compressor` and `decompressor` are compatible functions (
        `compressor(decompressor(x)) = decompressor(compressor(x)) = x` for
        every `x`) and `name` is a name to assign to the codec represented by
        the two functions.
        """
        self._codecs = {
            name: (c, d) for (name, c, d) in codecs
        }

    def __getattr__(self, name):
        prefix, suffix = name.split('_')

        if prefix in self._codecs:
            if suffix == 'compressor':
                return self._codecs[prefix][0]
            elif suffix == 'decompressor':
                return self._codecs[prefix][1]
            else:
                raise AttributeError('Attribute named %s not found' % name)
        else:
            raise AttributeError('Codec named %s not found' % prefix)
