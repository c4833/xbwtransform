from core.xbw.codec.CodecBundle import CodecBundle


class SpineCodecBundle(CodecBundle):
    def __init__(
            self, label_compressor, label_decompressor
    ):
        super().__init__(
            ('label', label_compressor, label_decompressor),
        )
