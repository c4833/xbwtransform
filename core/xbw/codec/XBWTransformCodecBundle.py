from core.xbw.codec.CodecBundle import CodecBundle


class XBWTransformCodecBundle(CodecBundle):
    def __init__(
            self,
            label_compressor, label_decompressor,
            last_compressor, last_decompressor
    ):
        super().__init__(
            ('label', label_compressor, label_decompressor),
            ('last', last_compressor, last_decompressor)
        )
