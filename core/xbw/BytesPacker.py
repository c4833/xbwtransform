class BytesPacker:
    """
    A utility class with the same intent of the `struct` package, but aimed at
    packing variable-length data.
    """
    def pack(self, *args):
        """
        :param args: An iterable of bytes
        :return:
        """
        return b''.join((
            str(len(args)).encode(),
            b'\n',
            b'\n'.join(
                str(len(a)).encode() for a in args
            ),
            b'\n',
            *args
        ))

    def unpack(self, archived_text):
        tokens, archived_text = archived_text.split(b'\n', 1)
        tokens = int(tokens.decode())

        lengths = list()

        while tokens > 0:
            length, archived_text = archived_text.split(b'\n', 1)
            lengths.append(int(length))

            tokens -= 1

        for length in lengths:
            yield archived_text[:length]

            archived_text = archived_text[length:]
