from core.xbw.BaseXBWEntry import BaseXBWEntry


class BaseXBWTransform:
    def __init__(self, entries):
        if any(filter(lambda e: not isinstance(e, BaseXBWEntry), entries)):
            raise TypeError('An iterable of BaseXBWEntry instances is expected')

        self._entries = entries

    def __repr__(self):
        return repr(self._entries)

    def __str__(self):
        return '\n'.join(map(str, self._entries))

    def __eq__(self, other):
        return self.__class__ == other.__class__ and \
               tuple(self._entries) == tuple(other._entries)

    def __add__(self, other):
        return self.__class__(
            *(self._entries + other._entries)
        )

    def __len__(self):
        return len(self._entries)

    def __iter__(self):
        return iter(self._entries)

    def __getitem__(self, index):
        if isinstance(index, (int, slice)):
            return self._entries[index]
        else:
            return self._entries[slice(*index)]

    @property
    def labels(self):
        return ''.join(
            map(
                lambda entry: entry.label,
                self
            )
        )

    @property
    def lasts(self):
        return ''.join(
            map(
                lambda entry: str(int(entry.last)),
                self
            )
        )
