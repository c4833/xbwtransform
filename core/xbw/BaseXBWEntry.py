class BaseXBWEntry:
    def __init__(self, is_last, label, suffix, is_leaf=False):
        self._last = bool(is_last)
        self._label = str(label)
        self._suffix = suffix
        self._is_leaf = bool(is_leaf)

    def __repr__(self):
        return str((
            int(self.last), self.label, self.suffix
        ))

    def __str__(self):
        return repr(self)

    def __le__(self, other):
        return self.suffix <= other.suffix

    def __lt__(self, other):
        return self.suffix < other.suffix

    def __eq__(self, other):
        return (self.last, self.label, self.suffix) == \
               (other.last, other.label, other.suffix)

    @property
    def last(self):
        return self._last

    @property
    def label(self):
        return self._label

    @property
    def suffix(self):
        return self._suffix

    @suffix.setter
    def suffix(self, value):
        self._suffix = value or ''

    @property
    def is_leaf(self):
        return self._is_leaf
