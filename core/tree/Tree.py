from core.tree.StringVisit import StringVisit


class _SentinelTree:
	def __init__(self):
		self._sibling = None

	@property
	def next_sibling(self):
		return self._sibling

	@next_sibling.setter
	def next_sibling(self, tree):
		self._sibling = tree


class Tree:
	"""
	An ordered tree implementation with arbitrary node values.
	"""
	def __init__(self, value=None, *children):
		self._value = value
		self._parent = None
		# First child sentinel
		self._first_child = _SentinelTree()
		self._sibling = None

		for c in children:
			self.append_child(c)

	def __iter__(self):
		"""
		:todo: Implement
		"""
		pass

	def __eq__(self, other):
		return type(other) == Tree and self.value == other.value and all(
			c1 == c2 for c1, c2 in zip(self.children, other.children)
		)

	@property
	def value(self):
		return self._value

	@value.setter
	def value(self, x):
		self._value = x

	@property
	def parent(self):
		"""
		:return: The parent node. This is `None` if `self` is the root node.
		"""
		return self._parent

	@property
	def first_child(self):
		return self._first_child.next_sibling

	@property
	def last_child(self):
		curr = self.first_child

		while curr and curr.next_sibling:
			curr = curr.next_sibling

		return curr

	@property
	def children(self):
		curr = self.first_child

		while curr:
			yield curr

			curr = curr.next_sibling

	def append_child(self, child):
		"""
		:param child: tree node to insert as the last (rightmost) child.
		:return: Inserted child
		"""
		if child.parent:
			child.parent.remove_child(child)

		child._parent = self
		last = self.last_child

		if last:
			last.next_sibling = child
		else:
			self._first_child.next_sibling = child

		return child

	def remove_child(self, child):
		"""
		Removes `child` from this tree. Comparison is made by identity.
		"""
		# prev initially points to the sentinel tree
		prev = self._first_child

		for curr in self.children:
			if curr is child:
				prev.next_sibling = curr.next_sibling
				curr._parent = None
				curr.next_sibling = None

				break

			prev = curr

	@property
	def next_sibling(self):
		"""
		:return: This tree next sibling node. `None` if it is the last of its parent (or the root node).
		"""
		return self._sibling

	@next_sibling.setter
	def next_sibling(self, sibling):
		"""
		Set this tree next sibling node, preserving siblings that were already present so that they come
		after `sibling.`
		"""
		if sibling:
			if sibling.parent and sibling.parent is not self.parent:
				sibling.parent.remove_child(sibling)

			sibling._parent = self._parent

		self._sibling = sibling

	def __repr__(self):
		return 'tree <%s, %s>' % (self.value, ''.join(
			map(repr, self.children)
		))

	def __str__(self):
		"""
		:return: The pseudo Simple tree representations of this tree.
		"""
		return StringVisit()(self)
