from core.tree.Tree import Tree


class SimpleTreeReader:
	def __init__(self, conversion=str):
		self._conv = conversion

	def readfile(self, filepath):
		with open(filepath) as infile:
			return self.read(infile.read(-1))

	def read(self, text):
		lines = text.splitlines()
		tokens = []

		if lines:
			root_level, root_value = self._initial_tabs_num(lines[0]), lines[0].strip()
			tokens.append(
				(0, root_value)
			)

			for line in lines[1:]:
				level = self._initial_tabs_num(line) - root_level

				if level <= 0:
					raise ValueError('One node is below or at the same root level')

				tokens.append(
					(level, line.strip())
				)

		return self._build_tree(tokens)

	def _build_tree(self, tokens):
		if tokens:
			prev_level, value = tokens[0]

			if prev_level != 0:
				raise ValueError('First token level must be 0')

			root = Tree(value)
			nodes = [root]

			for level, value in tokens[1:]:
				if level <= 0:
					raise ValueError('Level of nodes after root cannot be <= 0')
				if level > prev_level + 1:
					raise ValueError('Too much difference')

				node = Tree(value)

				for _ in range(level, prev_level + 1):
					nodes.pop()

				nodes[-1].append_child(node)
				nodes.append(node)
				prev_level = level

			return root

		return None

	@staticmethod
	def _initial_tabs_num(string):
		count = 0

		for c in string:
			if c == '\t':
				count += 1
			else:
				break

		return count
