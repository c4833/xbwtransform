class PreorderVisit:
    """
    A callable, pre-order visit class for `tree`. Extending classes are expected
    to implement `_process_node()` and `_aggregate()`, and may optionally override `_accumulate()` to accommodate
    their needs.
    """
    def __init__(self, depth=0):
        self._depth = depth

    def __call__(self, tree):
        return self._visit(tree, self._depth)

    def _visit(self, tree, acc):
        # root output
        root = self._process_node(tree, acc)
        # children output
        children = tuple(map(
            lambda c: self._visit(c, self._accumulate(acc, tree)), tree.children
        ))

        return self._aggregate(root, children) if children else root

    def _process_node(self, tree, depth):
        """
        :return: The mapped value of a node represented by `tree`. This method shall only process
        one node at a time, with no consideration for subtrees.
        """
        raise NotImplemented()

    def _accumulate(self, depth, tree):
        """
        :return: An auxiliary value that can be used by `_process_node()`. The default implementation returns the
        current node depth, whose value is fed into the `depth` parameter of `_process_node()`. Deriving
        subclasses may override this method with any behavior they wish.
        """
        return depth + 1

    def _aggregate(self, root, children):
        """
        :param children: The children processes output, expressed as a `tuple`
        :return: The "aggregate" value of a node `root` and its children mappings that were previously mapped
        by `_process_node`.
        """
        raise NotImplemented()
