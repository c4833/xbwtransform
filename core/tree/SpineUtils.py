from core.tree.Tree import Tree


def build_left_spine(values):
    if values:
        parent = root = Tree(values[0])
        index, length = 1, len(values)

        if len(values) % 2 == 0:
            raise ValueError('The values sequence must have an odd length')

        while index < length:
            new_parent = Tree(values[index])
            leaf = Tree(values[length - index])

            parent.append_child(new_parent)
            parent.append_child(leaf)

            parent = new_parent
            index += 1

        return root

    return None


def build_right_spine(values):
    if values:
        parent = root = Tree(values[0])
        index, length = 1, len(values)

        if len(values) % 2 == 0:
            raise ValueError('The values sequence must have an odd length')

        while index < length:
            new_parent = Tree(values[index])
            leaf = Tree(values[index + 1])

            parent.append_child(new_parent)
            parent.append_child(leaf)

            parent = new_parent
            index += 2

        return root

    return None
