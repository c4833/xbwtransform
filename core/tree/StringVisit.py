from core.tree.PreorderVisit import PreorderVisit


class StringVisit(PreorderVisit):
    """
    A `PreorderVisit` producing pseudo Simple tree representations of `tree` instances.
    """
    def _process_node(self, tree, depth):
        return depth * '\t' + str(tree.value)

    def _aggregate(self, root, children):
        return root + '\n' + '\n'.join(children)
