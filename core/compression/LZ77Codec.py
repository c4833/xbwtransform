from math import ceil, log


class LZ77Codec:
    """
    A character-based LZ77 codec. This codec expects its input to be in a
    non-binary format (i.e. extended ASCII) and emits its compressed text in a
    non-binary format as well. It is not designed to provide an actual LZ77
    compression/decompression implementation, but is meant to offer an
    experimental analysis and debugging support.
    """
    def __init__(self, *, buffer=1024, look_ahead=512):
        self._buffer = int(buffer)
        self._look_ahead = int(look_ahead)

        if self._buffer < self._look_ahead:
            raise ValueError('The look ahead value must be < the buffer one')

        self._len_size = ceil(log(self._look_ahead, 10))
        self._pointer_size = ceil(log(self._buffer - self._look_ahead, 10))

    @property
    def block_size(self):
        """
        :return: The number of characters needed to encode one output block.
        """
        return self._len_size + self._pointer_size + 1

    def compress(self, source):
        # curr = i if we are reading from the ith symbol onward
        curr = 0

        while curr < len(source):
            rear = max(0, curr - self._buffer + self._look_ahead)
            pointer, length = curr, 0

            for i in range(rear, curr):
                new_length = self._run_length(source, i, curr)

                if length < new_length:
                    length = new_length
                    pointer = i

            next_char = source[curr + length] if curr + length < len(source) \
                else ''

            yield self._encode(length, curr - pointer, next_char)

            curr += length + 1

    def decompress(self, compressed_text):
        curr = 0
        buffer = ''

        while curr < len(compressed_text):
            length_end = curr + self._len_size
            pointer_end = length_end + self._pointer_size

            length = int(compressed_text[curr:length_end])
            pointer = int(compressed_text[length_end:pointer_end])
            last_char = compressed_text[pointer_end]\
                if pointer_end < len(compressed_text) else ''

            while length > 0:
                buffer = buffer + buffer[-pointer]
                # pointer needs not be updated, since buffer is extended at
                # every loop
                length -= 1

            buffer = buffer + last_char
            curr += self.block_size

        return buffer

    def _run_length(self, source, i, j):
        length = 0

        if j < i:
            raise ValueError('i is greater than j')

        while j < len(source) and source[i] == source[j] \
                and length <= self._look_ahead:
            i += 1
            j += 1
            length += 1

        return length

    def _encode(self, length, pointer, next_char):
        return ''.join((
            str(length).zfill(self._len_size),
            str(pointer).zfill(self._pointer_size),
            next_char
        ))
