#!/usr/bin/env python3
import argparse

from parser_commands.lz.AnalyzeLCFsCommand import CompressLCFsCommand
from parser_commands.lz.GZIPAnalyzeLCFsCommand import GZIPCompressLCFsCommand
from parser_commands.lz.LZ77CompressCommand import LZ77CompressCommand
from parser_commands.lz.LZ77DecompressCommand import LZ77DecompressCommand
from parser_commands.lz.PrintTableCommand import PrintTableCommand
from parser_commands.lz.WriteLCFsCommand import WriteLCFsCommand
from parser_commands.wrap_command import wrap_command


def _build_parser():
    parser = argparse.ArgumentParser(
        description='LZ1 compression, decompression and other utilities.'
    )
    subparsers = parser.add_subparsers(
        dest='subcommand', required=True
    )
    commands = [
        # Compression utilities
        LZ77CompressCommand,
        LZ77DecompressCommand,
        # Analysis utilities
        PrintTableCommand,
        WriteLCFsCommand,
        CompressLCFsCommand,
        GZIPCompressLCFsCommand,
    ]

    for c in commands:
        p = c.set_up_parser(
            subparsers.add_parser(
                c.COMMAND_NAME,
                description=c.DESCRIPTION
            )
        )
        p.set_defaults(func=wrap_command(c))

    return parser


def main():
    parser = _build_parser()
    args = parser.parse_args()

    return args.func(args)


if __name__ == "__main__":
    exit(main())
