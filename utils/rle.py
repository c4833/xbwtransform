def rle(_input):
    if len(_input) <= 1:
        return _input
    else:
        output = ''
        i, count = 1, 1

        while i <= len(_input):
            if i < len(_input) and _input[i] == _input[i - 1]:
                count += 1
            else:
                factor = str(count) if count > 1 else ''
                output += factor + _input[i - 1]

                count = 1

            i += 1

        return output
