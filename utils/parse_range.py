def parse_range(s, separator=':'):
    ints = tuple(map(int, s.split(separator)))

    if len(ints) not in (1, 2, 3):
        raise ValueError(
            'The input string should consist of three parseable integers'
        )

    return range(*ints)
